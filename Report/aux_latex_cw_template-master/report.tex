% #######################################
% ########### FILL THESE IN #############
% #######################################
\def\mytitle{Data Analytics Report}
\def\mykeywords{Fill, These, In, So, google, can, find, your, report}
\def\myauthor{40082562}
\def\contact{40082562@napier.ac.uk}
\def\mymodule{Data Analytics (SET09120)}
% #######################################
% #### YOU DON'T NEED TO TOUCH BELOW ####
% #######################################
\documentclass[12pt, a4paper]{article}
\usepackage[a4paper,outer=1.5cm,inner=1.5cm,top=1.75cm,bottom=1.5cm]{geometry}
\twocolumn
\usepackage{amsmath}
\usepackage{graphicx}
\graphicspath{{./images/}}
%colour our links, remove weird boxes
\usepackage[colorlinks,linkcolor={black},citecolor={blue!80!black},urlcolor={blue!80!black}]{hyperref}
%Stop indentation on new paragraphs
\usepackage[parfill]{parskip}
%% Arial-like font
\IfFileExists{uarial.sty}
{
    \usepackage[english]{babel}
    \usepackage[T1]{fontenc}
    \usepackage{uarial}
    \renewcommand{\familydefault}{\sfdefault}
}
%{
%    \GenericError{}{Couldn't find Arial font}{ you may need to install 'nonfree' fonts on your system}{}
%    \usepackage{lmodern}
%    \renewcommand*\familydefault{\sfdefault}
%}
%Napier logo top right
\usepackage{watermark}
%Lorem Ipusm dolor please don't leave any in you final report ;)
\usepackage{lipsum}
\usepackage{xcolor}
\usepackage{listings}
%give us the Capital H that we all know and love
\usepackage{float}
%tone down the line spacing after section titles
\usepackage{titlesec}
%Cool maths printing
\usepackage{amsmath}
%PseudoCode
\usepackage{algorithm2e}

\titlespacing{\subsection}{0pt}{\parskip}{-3pt}
\titlespacing{\subsubsection}{0pt}{\parskip}{-\parskip}
\titlespacing{\paragraph}{0pt}{\parskip}{\parskip}
\newcommand{\figuremacro}[5]{
    \begin{figure}[#1]
        \centering
        \includegraphics[width=#5\columnwidth]{#2}
        \caption[#3]{\textbf{#3}#4}
        \label{fig:#2}
    \end{figure}
}

\lstset{
	escapeinside={/*@}{@*/}, language=C++,
	basicstyle=\fontsize{8.5}{12}\selectfont,
	numbers=left,numbersep=2pt,xleftmargin=2pt,frame=tb,
    columns=fullflexible,showstringspaces=false,tabsize=4,
    keepspaces=true,showtabs=false,showspaces=false,
    backgroundcolor=\color{white}, morekeywords={inline,public,
    class,private,protected,struct},captionpos=t,lineskip=-0.4em,
	aboveskip=10pt, extendedchars=true, breaklines=true,
	prebreak = \raisebox{0ex}[0ex][0ex]{\ensuremath{\hookleftarrow}},
	keywordstyle=\color[rgb]{0,0,1},
	commentstyle=\color[rgb]{0.133,0.545,0.133},
	stringstyle=\color[rgb]{0.627,0.126,0.941}
}

\thiswatermark{\centering \put(336.5,-38.0){\includegraphics[scale=0.8]{logo}} }
\title{\mytitle}
\author{\myauthor\hspace{1em}\\\contact\\Edinburgh Napier University\hspace{0.5em}-\hspace{0.5em}\mymodule}
\date{}
\hypersetup{pdfauthor=\myauthor,pdftitle=\mytitle,pdfkeywords=\mykeywords}
\sloppy
% #######################################
% ########### START FROM HERE ###########
% #######################################
\usepackage{fontspec}
\setmainfont{Arial}
\begin{document}
\onecolumn
\vspace*{250pt}
    {\let\newpage\relax\maketitle}
\newpage

\tableofcontents

\newpage
\twocolumn
\section{Introduction}

Throughout this report the concept of data analysis will be implemented to perform exploratory mining of the given dataset. The data set contains historical data gathered by a German bank. The aim of this report is to analyse the data using methods such as \textit{‘Classification}, \textit{‘Regression’}, \textit{‘Association’} and \textit{‘Clustering’}. Visualisation of the data will also be used to find patterns in the data that may be indicative of trends or form a guide to how the bank decides if the credit should be given or not.

The data set provided contain inconsistencies as do most publicly available data sets. These inconsistencies comprise of miss-spelling of words, errors when entering numeric values and miss classification of attributes such as job. Due to this, the report will also explore the methods used when cleaning `dirty' data. Throughout this section, the need to convert data will be discussed and methods of doing so will be implemented.
Data analytics and the methods used will also be explored and the reasoning behind the choice of algorithms will be discussed.



\section{Data Preparation}
For the purpose of this report, the data will first be cleaned and prepared. During this stage the data set was initially examined using excel to ensure the basic structure of the data was sound. At this point, column headers were added to allow the columns to be easily identified later; the column headers were set to the attribute names provided for this coursework. The attributes are named as follows: \textit{case\_no, checking\_status, credit\_history, purpose, credit\_amount, saving\_status, employment, personal\_status, age, exsisting\_credits, job, num\_dependancies} and \textit{class}.
 
The data set provided was loaded into the application OpenRefine, which allows for the cleaning of “dirty” data. The application will read in data in the form of an excel spread sheet and once cleaning of the data is complete the user may export this “clean” data as a Comma Separated Values (CSV) file.
Throughout the process of preparing the data multiple anomalies were found. These anomalies included spelling errors, capitalisation of words where it was not needed and instances where the \textit{credit\_amount} was above one hundred million (see Appendix 1).

Where the credit amount of more than 1 million the trailing zeros were removed and the values brought back into line with the rest of the data that was provided. This was done as the values seemed too high in comparison to the rest of the data and this was likely recorded in error. One other instance of a data value that seemed to be outside of the range for this data was the \textit{credit\_ammount} of 111328, for this coursework it was changed to 1328.

The first task that was undertaken during the data preparation was to re-name the attribute values. During this step, the various attribute values that contained spaces were found and the space was replaced with the character \textbf{`\_'}. This change allowed for the values to be read as single values by the data mining software called Weka.  

Once the data was cleaned it was exported as a CSV file. This was then opened in a word editor and the attribute names and values set (see Appendix 2). Once these attributes and values were defined, the file extension was changed to \textbf{textit{.arff}}.  This data set uses a mixture of nominal and numeric attributes and will only work with \textit{J48} and \textit{OneR} classifiers.

In order to allow Weka to use the data with the \textit{ID3} classification the data had to be converted again to only include nominal data. This was carried out in Weka using the \textit{NumericToNominal} filter. The data was then exported as a \textit{.arff} file for use with the ID3 classifier.

\section{Data analysis} 

Throughout this section of the report the data will be analysed using three techniques. These techniques consist of classification, regression, association and clustering. 

\subsection{Association}
The first technique used was the association algorithm, \textit{Apriori}. The \textit{Apriori} algorithm is a commonly used datamining algorithm. It is used primarily in the mining of frequent items sets and how these relate to other factors within the data. For example, if a customer buys onions and potatoes, the customer would typically also buy burgers.

For this experiment the \textit{metricType} was set to \textit{Confidence}. This will allow for the application to determine the factors that will give the highest chance for a loan being approved. The confidence algorithm can be defined as;

\[ con f(x \rightarrow y)=\frac{supp(x \cup y)}{supp(x)} \]

This algorithm was used to find the association rules for the attribute \textit{`class'}, this attribute contains the values \textit{`good'} (the bank would approve this credit) and \textit{`bad'} (the bank would not approve the credit). The aim of the association method was to find rules that gave the class attribute a value of \textit{`good'} which means the bank would approve the credit. In order to get the results to be checked against the class, the default \textit{Apriori} algorithm was altered within Weka (see Appendix 3). The value of \textbf{‘car’} was changed to \textit{true} to allow the association rules to be mined against the class attribute. 

For the purpose of this experiment, the \textit{‘minMetric’} was set to \textit{0.1} allowing a larger number of association rules to be found. The number of rules to be found \textit{(‘numRules’)} was limited to 400. Therefore the \textit{‘lowerBoundMinSupport} was set to a value of \textit{0.1}. 

From this data, the likelihood of an applicant being offered the credit was checked against the status of their current account held at the bank (see Appendix 4). This information shows that applicants with a value of \textit{no\_checking} (no current account in bank) are the most likely to have their credit application approved with a confidence level of \textit{0.88}. This value is derived by dividing the number of applicants with \textit{ checking\_status=no\_checking and class = good} by the number of \textit{ checking\_status=no\_checking} this is shown using the following equation; 
\begingroup\makeatletter\def\f@size{8}\check@mathfonts
\[ con f(checking\_status \Rightarrow good)=\frac{supp(\{checking\_status, good\})}{supp(\{good\})}\]
\endgroup
From the results of this, the coverage can be determined as being 39.4\% of the dataset. The number of results is the base figure and this can be further broken down into smaller groups if, for example the rule is changed to include \textit{personal\_status} or if the rule is set to, \textit{checking\_status=no\_checking} and \textit{personal\_status=male\_single} \textbf{->} \textit{class=good}. 

 The result then has a confidence value of \textit{0.9} as 208 of the 232 applicants are assumed to be good to provide credit to. However, the coverage of this rule is lower as only 232 of the 1000 data items are being considdered, this equats to 23.2\% of the data.
 
From the use of the \textit{Apriori} algorithm, it can be determined that applicants who do not have a current account in the bank are more likely to be approved for credit when compared to those who have a current account in the bank. 
\linebreak

\subsection{Classification}

For the classification of data, the \textit{Iterative Dichotomiser 3} (ID3) algorithm was used to analyse the provided data set. For this algorithm to work the data was converted to include only nominal values as discussed in section 1 – data preparation.  This algorithm uses entropy to measure the amount of uncertainty in the dataset. Entropy is shown using the following algorithm;

\[ H(S) = \sum_{\chi \epsilon X} - p(x) \log_2 p(x) \]

During the first experiment, the data was analysed using the \textit{ID3} algorithm and the training set the results of the predictions would return 100\% accuracy for each attribute that was tested under these conditions.Due to this it was decided that the \textit{OneR} algorithm would be used as the classification algorithm.

To carry out a more in-depth classification scenario \textit{RStudio} was used to apply tailored rules to the data set. To carry this out, the original data set was loaded into the \textit{RStudio} program. For this experiment the \textit{‘party’} library package was used. 

Once loaded, the data was split into two sub sets of data, \textit{‘train.data’} and \textit{“test.data’} split 70, 30 respectively. Several rules were set out for use on the data as follows;

\textbf{‘credf’:} \newline\textit{credf <- class ~ personal\_status + job + credit\_history}  \newline
\textbf{‘statusf’ :} \newline\textit{statusf <- personal\_status ~ employment + class }\newline
\textbf{‘gbf’ :} \newline\textit{gbf <- class ~ credit\_history + credit\_amount + savings\_status}\newline
\textbf{‘gbpersonal’:}\newline\textit{gbpurpose <- class ~ personal\_status+job+existing\_credits}\newline
\textbf{‘gbsavings’:}\newline\textit{ gbsaving <- class ~ savings\_status + personal\_status + credit\_amount + checking\_status }

Each of these rules were run against the training data and the results observed. The first rule \textbf{‘credf’} was run and the data compared with the class attribute to see how well this rule would predict whether the applicant would be approved for credit or not. This rule had misidentified 28 of the 64 \textit{‘bad’} applications and 156 of the 634 \textit{‘good’} applications. This gave the rule an accuracy of 73.6\% for the data that the test was run against. From this data, a classification tree was built (see Appendix 5) to provide a visual representation of the classification breakdown. To keep the tree clean, the number of child nodes was kept to a value of two for all parent nodes. 

The second rule \textbf{‘statusf”} was run on the \textit{train.data} this training data was run against the \textit{personal\_status} attribute to see if the personal status could be accurately predicted using the \textit{class} and \textit{employment} attributes. The results of this test conclude that this is not an accurate test for determining the \textit{personal\_status} of the applicant. It did however give an indication of the relationship between \textit{personal\_status} and \textit{class}. The results showed that of the 130 applicants who are \textit{‘female\_div/dep/mar’} 46 (35\%) were considered unsafe to offer credit too. This was compared to the 568 \textit{single males} of which 146 (25\%) were considered unsafe to offer the credit too. Based on the result from the \textit{train.data} set we ca assume that single males are more likely to be considered safe to offer credit to.

From the five rules outlined in this section, the most accurate was the \textbf{‘gbf’} rule. This considered the attributes \textit{credit\_history}, \textit{credit\_amount} and \textit{saving\_status}. This gave a total accuracy of 75.7\% with 34 of the 90 bad applicants being misclassified and 136 of the 608 good applications also being misclassified (see Appendix 6).

\subsection{Clustering}

Clustering is the task of grouping a set of objects into the same group or a cluster. Objects in these groups are more similar to each other in one way or another than to those in other groups. 

For this coursework the \textit{SimpleKMeans} algorithm was used. The same data was passed through the algorithm six times each with a different class for the cluster to evaluate. For the output the data was clustered into five clusters. 

For this data I will analyse the clusters that give a class of \textit{‘bad’} and compare them with other clusters values to attempt to find a correlation between the class \textit{‘bad’} and attribute values such as \textit{personal status}, \textit{existing credits}, \textit{job} and \textit{credit history}. 

 The first to be passed through the clustering algorithm was the test data. This took all 1000 elements of data and separated them into clusters (see appendix 7). From this data, the fifth cluster was given a class of \textit{‘bad’}. Within this cluster the personal status was male and single. The number of existing credits was 1.3(mean) their job was skilled, and their credit history was existing and paid. When this is compared to the rest of the clusters in this test, two other clusters had the same mean employment time of more than seven years and credit history that was \textit{‘existing paid’}. One of the clusters is \textit{‘skilled’} and the other is \textit{‘unskilled resident’}. Both other clusters had a checking status of \textit{‘no checking’}.

The data was then passed through the \textit{‘SimpleKMeans’} algorithm and the results were evaluated based on the \textit{credit history} attribute. This was the only change to the test. The results of this data showed that a single male who has held his skilled job for more than seven years was given a class of \textit{‘bad’} the credit history for this cluster was set to \textit{‘all paid’} as was the example in the test data.  

This pattern can be seen again when looking at the data set where \textit{saving status} was used as the attribute to evaluate. Again, a single male who has held his skilled job for more than seven years was given a class of \textit{‘bad’}. In all three instances of this so far, the purpose of the credit was for a new car. The mean number of loans for these clusters has been around 1.3. and the requested credit amount had a mean value of above £4000.


From this data, we can expect that a single male with a job of more than 7 years with all debts being paid or up to date on payments and who is asking for credit of around 4000 will be given the class of \textit{‘bad’} 

During this experiment the values of \textit{personal status} and \textit{existing credits} were compared and a cluster diagram produced. This diagram was then split to view only those who received a class of \textit{‘good’} (see Appendix 8) and those who received a class of \textit{‘bad’} (see Appendix 9). These were compared to see if there was any correlation between personal status and current credits that swayed the decision of whether the bank would approve the credit.

From these graphs, we can see that there are a higher number of \textit{‘bad’} applications for females that are divorced, separated or married compared to the amount of \textit{‘good’} applications for the same group. It is also visible that the number of single male applicants with more than one existing credit are considered \textit{‘good’} to provide credit too, compared to their female counterparts. 

\section{Data Visualisation}

During this section of the report the data will be visualised for the purpose of data exploration. The aim of this is to find out if there is a correlation between the applicants employment status and their personal status have on the loan decision. 

The main tool used to visualise the data will the Rstudio using the GGplot library. These tools were chosen as they offer a diverse range of options when plotting the data, making it a valuable tool when analysing the visual patterns within data. 

During this experiment, three visual methods were used to explore the data and any correlation between the various attributes and the decision made by the bank. The attributes that will be compared during this stage of the data exploration are \textit{credit\_history} with \textit{checking\_status}, \textit{credit\_amount} with \textit{purpose} and finally \textit{age} with \textit{saving\_status}.

The first comparison to be made was between \textit{age} and \textit{saving\_status}. A jitter plot was implemented in this instance so that the data could be clustered and compared. The following encoding was used to produce this graph;

\begin{lstlisting}[caption= ggplot - Age and Savings shown by Class]
ggplot(‘credit_Data_Cleaned’, aes(x=savings_status, y=Age, color=class)) + geom_jitter()
\end{lstlisting}

This encoding produced a graph that grouped the age of the applicant into bands depicted by their savings at the time of applying for credit. Within these clusters, the points plotted on the graph are further defined into the decision made by the bank, either \textit{`good'} or \textit{`bad'}. Splitting the data like this allows for easy interpretation and reading. From the data displayed during this test, we can see that the age and saving status of the applicant does not have much effect on the decision made by the bank. 

The next set of attributes to be examined were the applicants \textit{credit\_history} and \textit{checking\_status}. These were again viewed using the \textit{geom\_jitter} function as the author feels this is the best way to represent this type of data. The encoding used is as follows;

\begin{lstlisting}[caption= ggplot – credit history and checking status shown by Class]
ggplot(‘credit\_Data\_Cleaned’, aes(x=checking\_status, y=credit\_history, color=class)) + geom\_jitter()
\end{lstlisting}

The data was again classified by the \textit{class} attribute. This was done to see the relationship between the banks decision and the values of the attributes. From this comparison it is evident that the majority of applicants that do not have an existing account \textit{‘no checking’} with the bank have a very high approval rate when compared with the other \textit{checking status} values. This can be demonstrated by using the applicants that have a \textit{checking status} of \textit{‘<0’} even if these applicants have paid all of their existing credits off \textit{(allpaid)}, it is still very likely that the bank will reject their application. This is also evident when comparing applicants who have existing credits that are being paid back duly \textit{(existing\_paid)} and the \textit{checking\_status}. Those who have accounts with the bank are more likely to have their application rejected if they have a \textit{checking\_status} of either \textit{‘<0’} or \textit{‘0<=X<200’}.

The final set of attributes to be compared are \textit{credit\_amount} and \textit{purpose}.  The following \textit{ggplot} encoding was used to produce the graph;

\begin{lstlisting}[caption= ggplot – credit history and checking status shown by Class]
ggplot(‘credit\_Data\_Cleaned’, aes(x=purpose, y=credit\_amount, color=class)) + geom\_jitter()
\end{lstlisting}

From this graph, the likelihood of the loan being approved can be seen based on the purpose and the amount requested in the application. Based on this the most likely \textit{`purpose'} to be approved is \textit{used car} followed by \textit{radio\/TV}. The most likely application to be rejected based on this comparison would be credit for a new car that is under £5000.

During the visual exploration of the data, the question being investigated is the relationship between the applicant’s personal status and employment on the decision made by the bank. Three sketches of how this data could be visualised have been prepared. For each of the three sketches the layouts and encodings will be discussed.

The first sketch (see Appendix 10) implements a clustering style of data visualisation. The data in this sketch has been clustered into groups using the attributes \textit{Personal\_status} along the $\chi $ axis and \textit{employment} along the $\gamma $ axis. The encoding used for this is as follows;

\begin{lstlisting}[caption= ggplot – personal status and employment shown by Class]
ggplot(‘credit\_Data\_Cleaned’, aes(x=personal\_status, y=employment, color=class)) + geom\_jitter()
\end{lstlisting}

Within the clusters the data was coloured according to \textit{class}. This gave a good visual representation of the question being asked as it shows both attributes and the number of \textit{good} and \textit{bad} applications there are in each cluster on the graph.

The second sketch (see Appendix 11) shows the number of \textit{good} and \textit{bad} applications based on the applicants \textit{personal\_status}. A bar plot was used to achieve this visual representation, the encoding for this graph is as follows;

\begin{lstlisting}[caption= ggplot – personal status barplot]
ggplot(`credit\_data\_cleaned`, aes(x=personal_status, y=Case_no, fill=class)) + geom_bar(stat = "identity", position=position_dodge())
\end{lstlisting}

This graph gives a good indication of the effects that the personal status of the applicant has on the decision made by the bank. However, it does not answer the question being asked in its entirety as there is no data being shown for the \textit{`employment'} of the applicant. Because of this, the graph was not used as a visual representation of the data.

The third and final sketch (see Appendix 12) shows the data again clustered as in the first sketch but visualised in a different way. The data is grouped by \textit{employment} on the $\chi $ axis and \textit{class} along the $\gamma $ axis. The data was displayed as a jitter plot using the \textit{personal\_status} of the applicant as the point on the graph. This was achieved using the following encoding;

\begin{lstlisting}[caption = ggplot – employment and class shown by personal status]
ggplot(`credit._data_cleaned.(2)`, aes(x=employment, y=class, color=personal_status)) + geom_jitter(position = position_jitter(0.2))
\end{lstlisting}

This visualisation gives a good representation of the data and answers the question well. However, this is not as clear as the first sketch where the clusters were smaller and had only two colour variables. 

Due to this, the first sketch will be used to visualise the data in RStudio. This decision was made as the data in sketch 1 is easy to read and gives a god visual representation of the question being asked. 

The visual representation of this can be seen in appendix 13. Implementing this style of graph has given a good indication on the number of \textit{good} and \textit{bad} applications as the data is clustered into the attributes detailed in the question. Viewing the data like this allows a clear indication of the groups most likely to be offered credit based on their \textit{personal\_status} and \textit{employment}. By looking at the results in the graph, it can be seen that males who are married or widowed and have worked for more than seven years are the most likely to have their credit approved. This is clear as within this cluster 100\% of the applicants have been approved for credit by the bank. 

Similarly, females that are married, divorced or separated and have worked for between four and seven years have less than a 25\% chance of  being denied credit by the bank.

\section{Conclusion}

To conclude this report, it is evident that there are a number of factors need to be considered when the bank makes their decision on the applicant’s credit request. Analysing this data both through algorithms and visual methods is key to finding patterns. Looking at the aspects of the data explored in section 4 of this report the \textit{employment} and \textit{personal\_status} of the applicant can have an effect on the decision. 

During the data analysis of the provided data set, it was discovered that single males produced the most applications and have the highest credit approval rate. This is evident when comparing attributes during the classification stage of the data analysis. When looking at the checking status against personal status it is suggested that applicants who have no current account at the bank are most likely to be approved for the credit. 

When considering all the attributes within the data set, the most likely case for an applicant to be approved for credit is a single male who has no current account with the bank and needs the credit to finance the purchase of a used car. This conclusion was drawn through the use of data visualisation using clusters. The way in which the data is displayed when using clusters is simple to read and interpret. 

\newpage
\onecolumn
\section{Appendix}

\subsection{Appendix 1}
\begin{figure}[h]
\centering
  \includegraphics[width=\textwidth,height=\textheight,keepaspectratio]{credit_amount_Over_1milA1.png}
  \caption{Credit amount over 1 million}
  \label{fig:Credit over 1 million}
\end{figure}
\newpage
\subsection{Appendix 2}
\begin{lstlisting}[caption = Attributes added to arrf file]
@relation CreditData


@attribute Case_no real
@attribute checking_status {<0, 0<=X<200, >=200, no_checking}
@attribute credit_history {no_credits/all_paid, allpaid, existing_paid, delayed_previously, critical/other_existing_credit}
@attribute purpose {new_car, used_car, furniture/equipment, radio/tv,domestic_appliance, repairs, education, vacation, retraining, business, other}
@attribute credit_amount real
@attribute savings_status {<100, 100<=X<500, 500<=X<1000, >=1000, no_savings}
@attribute employment {unemployed, <1, 1<=X<4, 4<=X<7, >=7}
@attribute personal_status {male_div/sep, female_div/dep/mar, male_single, male_mar/wid, female_single,}
@attribute Age real
@attribute existing_credits real
@attribute job {unemp/unskilled_non_res, unskilled_resident, skilled, high_qualif/self_emp/mgmt}
@attribute num_dependents real
@attribute class {good, bad}

@data
1,<0,critical/other_existing_credit,radio/tv,1169,no_savings,>=7,male_single,67,2,skilled,1,good
\end{lstlisting}
\newpage
\subsection{Appendix 3}
\begin{figure}[h]
\centering
  \includegraphics[width=\textwidth,height=\textheight,keepaspectratio]{ApriorieditedA3.png}
  \caption{Edited Apriori Alogrithm}
  \label{fig:Edited Apriori Alogrithm}
\end{figure}
\newpage
\subsection{Appendix 4}
\begin{figure}[h]
\centering
  \includegraphics[width=\textwidth,height=\textheight,keepaspectratio]{AprioriresultsA4.png}
  \caption{Results of Apriori Alogrithm}
  \label{fig:Results of Apriori Alogrithm}
\end{figure}
\newpage
\subsection{Appendix 5}
\begin{figure}[h]
\centering
  \includegraphics[scale=0.5]{Rplot_credf.png}
  \caption{Credf Classification Tree}
  \label{fig:Credf Classification Tree}
\end{figure}
\newpage
\subsection{Appendix 6}
\begin{figure}[h]
\centering
  \includegraphics[width=\textwidth,height=\textheight,keepaspectratio]{AprioriresultsA5.png}
  \caption{Results of Apriori Alogrithm}
  \label{fig:Results of Apriori Alogrithm}
\end{figure}
\newpage
\subsection{Appendix 7}
\begin{figure}[h]
\centering
  \includegraphics[width=\textwidth,height=\textheight,keepaspectratio]{Clusteringchecking_status6.png}
  \caption{Clustering based on Checking status}
  \label{fig:Clustering based on Checking statu}
\end{figure}
\newpage
\subsection{Appendix 8}
\begin{figure}[h]
\centering
  \includegraphics[width=\textwidth,height=\textheight,keepaspectratio]{clusteringexsistingcreditvpersonalstatus_good.png}
  \caption{existing credits vs. personal status(good)}
  \label{fig:existing credits vs. personal status(good)}
\end{figure}
\newpage
\subsection{Appendix 9}
\begin{figure}[h]
\centering
  \includegraphics[width=\textwidth,height=\textheight,keepaspectratio]{clusteringexsistingcreditvpersonalstatus_bad.png}
  \caption{existing credits vs. personal status(bad)}
  \label{fig:existing credits vs. personal status(bad)}
\end{figure}
\newpage
\subsection{Appendix 10}
\begin{figure}[h]
\centering
  \includegraphics[scale = 0.2]{DataAnalytics_Sketch1.jpg}
  \caption{Data Visualisation Sketch 1}
  \label{fig:Data Visualisation Sketch 1}
\end{figure}
\newpage
\subsection{Appendix 11}
\begin{figure}[h]
\centering
  \includegraphics[width=\textwidth,height=\textheight,keepaspectratio]{DataAnalytics_Sketch2.jpg}
  \caption{Data Visualisation Sketch 2}
  \label{fig:Data Visualisation Sketch 2}
\end{figure}
\newpage
\subsection{Appendix 12}
\begin{figure}[h]
\centering
  \includegraphics[width=\textwidth,height=\textheight,keepaspectratio]{DataAnalytics_Sketch3.jpg}
  \caption{Data Visualisation Sketch 3}
  \label{fig:Data Visualisation Sketch 3}
\end{figure}
\newpage

\end{document}